note
	description: "Summary description for {RECTANGLE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

---------------------------------------------------------
-- Caso 1:
-- Reference Type
---------------------------------------------------------

class
	RECTANGLE
inherit
	SHAPE

create
	make_rectangle

feature {NONE} -- Initialization
	make_rectangle(s1, s2 : INTEGER)
		do
			side1 := s1
			side2 := s2
		end

feature

--	tmp :   INTEGER -- This is added to force alignment (the total bytes have to be 16, because it is 8 aligned on a 64-bit system)
	side1 : INTEGER --REAL
	side2 : INTEGER --REAL

	perimenter : INTEGER do --REAL do
		Result := side1 * 2 + side2 * 2
	end

	area : INTEGER do
		--REAL do
		Result := side1 * side2
	end

	set_side1(s : INTEGER ) do --REAL) do
		side1 := s
	end

	set_side2(s : INTEGER ) do --REAL) do
		side2 := s
	end
end








---------------------------------------------------------
-- Caso 2:
-- Value Type
---------------------------------------------------------

--expanded class
--	RECTANGLE
--inherit
--	SHAPE

--feature

----	tmp :   INTEGER -- This is added to force alignment (the total bytes have to be 16, because it is 8 aligned on a 64-bit system)
--	side1 : INTEGER --REAL
--	side2 : INTEGER --REAL

--	perimenter : INTEGER do --REAL do
--		Result := side1 * 2 + side2 * 2
--	end

--	area : INTEGER do
--		--REAL do
--		Result := side1 * side2
--	end

--	set_side1(s : INTEGER ) do --REAL) do
--		side1 := s
--	end

--	set_side2(s : INTEGER ) do --REAL) do
--		side2 := s
--	end
--end
