note
	description : "Inheritance_Liskov application root class"
	date        : "$Date$"
	revision    : "$Revision$"


-- Revisar esta discusion de la lista de Eiffel.
-- https://groups.yahoo.com/neo/groups/eiffel_software/conversations/topics/14026
-- Es del 2009, pero hacen una comparacion de Bubble sort entre Eiffel y C.
-- Mi punto es que no tiene sentido comparar un algoritmo como Bubble sort, ya que es un algoritmo inutil, que solo sirve para
--   ense�ar como escribir un algoritmo ineficiente. Es un algoritmo que no deberia ensenarse nunca mas.
-- La comparacion deberia hacerse usando Quicksort, Merge-Sort, Heap-Sort, Insertion-Sort... etc...


class
	APPLICATION

inherit
	ARGUMENTS

create
	make



feature

--	test_original
--		local
--			square : SQUARE
--			rectangle : RECTANGLE
--		do

--			create square
--			create rectangle

--			square.set_side1(15)
--			square.set_side2(15)
----			square.set_side(15)


--			rectangle.set_side1 (33)
--			rectangle.set_side2 (22)


--			print(square.area)
--			print("%N")
--			print(rectangle.area)
--			print("%N")

--			square.hola()


--		end



	create_arrayed_list_square(max : INTEGER) : ARRAYED_LIST[SQUARE]
		local
			i : INTEGER
			temp_square : SQUARE
			side : INTEGER

--			rnd : SALT_XOR_SHIFT_64_GENERATOR
			rnd : SALT_DEVELOPER_RANDOM_GENERATOR

			--http://www.eiffelroom.org/blog/jocelyn_fiat/lets_talk_about_crypto_library
			--https://svn.eiffel.com/eiffelstudio/trunk/Src/unstable/library/text/encryption/crypto/salt/salt_xor_shift_64_generator.e
		do
			create Result.make(max)
			create rnd.make(1000)  		--TODO: a_length: INTEGER_32  ???? que significa length???

			from
				i := 0
			until
				i = max
			loop
				side := rnd.new_random.to_integer_32
				print("new random: ")
				print(side)
				print("%N")

				-- https://svn.eiffel.com/eiffelstudio/trunk/Src/unstable/library/text/encryption/crypto/salt/salt_xor_shift_64_generator.e
				create temp_square.make_square(side)
				Result.put_front (temp_square)
				i := i + 1
        	end
		end


	time_it(data : ARRAYED_LIST[SQUARE]; f: FUNCTION [APPLICATION, TUPLE, INTEGER] )
		local
			start : DATE_TIME
			eend : DATE_TIME
			res : INTEGER
			sorter: COLLECTION_SORTER[INTEGER];
		do

			create start.make_now

			-- Process

--			res := f

			create eend.make_now

		end


	test_arrayed_list_timming(max : INTEGER)
		local
			squares : ARRAYED_LIST[SQUARE]
		do

			squares := create_arrayed_list_square(max)

--			time_it(squares)   --TODO
		end


feature {NONE} -- Initialization



	make
		local
			max : INTEGER
		do

			max := 1000000

			--test_original()
			test_arrayed_list_timming(10)
--			test_array_timming(max)
--			test_special_timming(max)

		end

end
