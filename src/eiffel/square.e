-- INTEGER   == INTEGER_32
-- CHARACTER == CHARACTER_8
-- NATURAL   == NATURAL_32
-- REAL      == REAL_32


	-- Significado de la Macro OBJSIZ, que sirve para calcular el tamano de un objeto en base a sus atributos.
	-- @OBJSIZ(0,      0,           0,           3          , 1      , 0, 0         , 0)]
	-- @OBJSIZ(STRING, CHARACTER_8, INTEGER_16, INTEGER_32  , REAL_32, 0, INTEGER_64, REAL_64)]
	--         ARRAY   BOOLEAN      NATURAL_16  CHARACTER_32              NATURAL_32
	--                 INTEGER_8                NATURAL
	--                 NATURAL_8

	--[@OBJSIZ(2,4,2,5,2,0,2,1)];};

note
	description: "Summary description for {SQUARE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

---------------------------------------------------------
-- Caso 1,
-- Reference Type
-- SQUARE hereda de RECTANGLE (desventaja: SQUARE
-- necesita solo un dato en memoria, y con esta forma,
-- tiene dos, side1 y side2, que son parte de RECTANGLE)
---------------------------------------------------------
class
	SQUARE
inherit
	RECTANGLE

create
	make_square

feature {NONE} -- Initialization
	make_square(s : INTEGER)
		do
			make_rectangle(s, 2)
		end

feature
	hello() do
		print("square_hello%N")
	end

end




---------------------------------------------------------
-- Caso 2,
-- Value Type
-- SQUARE hereda de RECTANGLE (desventaja: SQUARE
-- necesita solo un dato en memoria, y con esta forma,
-- tiene dos, side1 y side2, que son parte de RECTANGLE)
---------------------------------------------------------
--expanded class
--	SQUARE
--inherit
--	RECTANGLE

----invariant
----	side1 = side2

--feature
--	hello() do
--		print("square_hello%N")
--	end

--end






---------------------------------------------------------
-- Caso 3,
-- Value Type
-- SQUARE es independiente de RECTANGLE
-- (ventaja: SQUARE solo tiene un dato en memoria Side
---------------------------------------------------------
--expanded class
--	SQUARE
--inherit
--	SHAPE

----invariant
----	side1 = side2

--feature

--	tmp :  INTEGER
--	side : INTEGER --REAL

--	perimenter : INTEGER do --REAL do
--		Result := side * 4
--	end

--	area : INTEGER do
--		--REAL do
--		Result := side * side
--	end

--	set_side(s : INTEGER ) do --REAL) do
--		side := s
--	end

--	hello() do
--		print("square_hello%N")
--	end
--end


---------------------------------------------------------
-- Caso 4,
-- Restriction inheritance.
-- Value Type
-- SQUARE hereda de RECTANGLE, pero en el invariante de
-- SQUARE se especifica que: side1 = side2
-- (desventaja: SQUARE
-- necesita solo un dato en memoria, y con esta forma,
-- tiene dos, side1 y side2, que son parte de RECTANGLE)
---------------------------------------------------------
--expanded class
--	SQUARE

--inherit
--	RECTANGLE

--feature
--	hello() do
--		print("square_hello%N")
--	end

--invariant
--	side1 = side2
--end
