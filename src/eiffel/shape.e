note
	description: "Summary description for {SHAPE}."
	author: ""
	date: "$Date$"
	revision: "$Revision$"

deferred class
	SHAPE

feature

	perimenter() : INTEGER --REAL
		deferred end

	area() : INTEGER --REAL
		deferred end

end
