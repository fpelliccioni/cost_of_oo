



// clang++ -O3 -std=c++1y cost_of_oo.cpp
// /Users/gordo/soft/llvm-build-release/Release+Asserts/bin/clang++ -O3 -std=c++1y -stdlib=libc++ -nostdinc++ -I/Users/gordo/soft/libcxx/include cost_of_oo.cpp
// g++ -O3 -std=c++11 cost_of_oo.cpp

// With linker optimization to strip the binary (not used code removal)
// /Users/gordo/soft/llvm-build-release/Release+Asserts/bin/clang++ -O3 -std=c++1y -Wl,-dead_strip -stdlib=libc++ -nostdinc++ -I/Users/gordo/soft/libcxx/include cost_of_oo.cpp


class rectangle {
private:
	double side1;
	double side2;

public:
	explicit rectangle(double side1, double side2)
		: side1(side1), side2(side2)
	{}
		
	double Side1() const
	{
		return side1;
	}

	double Side2() const
	{
		return side2;
	}

	double perimeter() const
	{
		return (2 * side1) + (2 * side2);
	}

	double area() const
	{
		return side1 * side2;
	}
};

struct square_inefficient : rectangle
{
// private:
// 	double dummy1;
// 	double dummy2;
// 	double dummy3;
// 	double dummy4;
// 	double dummy1b;
// 	double dummy2b;
// 	double dummy3b;
// 	double dummy4b;
    explicit square_inefficient(double side) : rectangle(side, side) {}
	//invariant (implicit): side1 == side2 
};

class square_efficient
{
private:
    double side;

public:
    explicit square_efficient(double side) : side(side) {}

	// inline
    double Side() const { return side; }
    // inline
    double perimeter() const { return 4 * side; }
    // inline
    double area() const { return side * side; }
};


class composite_inefficient
{
private:
    square_inefficient square1;
    square_inefficient square2;
    square_inefficient square3;

public:
    explicit composite_inefficient(double side_square1, double side_square2, double side_square3)
    	: square1(side_square1), square2(side_square2), square3(side_square3)
    {}

	// inline
    double area() const
    {
        return square1.area() + square2.area() + square3.area();
    }


    double perimeter() const
    {
        return square1.perimeter() + square2.perimeter() + square3.perimeter();
    }
};

class composite_efficient
{
private:
    square_efficient square1;
    square_efficient square2;
    square_efficient square3;

public:
    explicit composite_efficient(double side_square1, double side_square2, double side_square3)
    	: square1(side_square1), square2(side_square2), square3(side_square3)
    {}

	// inline
    double area() const
    {
        return square1.area() + square2.area() + square3.area();
    }
    double perimeter() const
    {
        return square1.perimeter() + square2.perimeter() + square3.perimeter();
    }
};

class composite_efficient2
{
private:
    double square1;
    double square2;
    double square3;

public:
    explicit composite_efficient2(double side_square1, double side_square2, double side_square3)
    	: square1(side_square1), square2(side_square2), square3(side_square3)
    {}

	// inline
    double area() const
    {
        return square1 * square1 + square2 * square2 + square3 * square3;
    }
    double perimeter() const
    {
        return square1 * 4 + square2 * 4 + square3 * 4;
    }
};



struct BigObject1 	//4 bytes * 4 = 16 bytes
{
	double a;
	double b;
	double c;
	double d;
};

struct BigObject2  // 16 bytes * 4 = 64 bytes
{
	BigObject1 a;
	BigObject1 b;
	BigObject1 c;
	BigObject1 d;
};

struct BigObject3  // 64 bytes * 4 = 256 bytes
{
	BigObject2 a;
	BigObject2 b;
	BigObject2 c;
	BigObject2 d;
};

struct SquareFer2
{
//		BigObject3 dummy;
//		BigObject2 dummy;
//		BigObject1 dummy;
	double dummy;
	long long dummy2;
	double side;

	double Side() const
	{
		return side;
	}

	double Dummy() const
	{
		return dummy;
	}
		
	explicit SquareFer2(double side)
	{
		side = side;
		dummy = side;

		dummy2 = 0;

//			_dummy = new BigObject3 ();
//			_dummy.a.a.a = side;
//			_dummy = new BigObject1 ();
//			_dummy.a = side;
	}

	double perimeter() const
	{
		return (2 * side) + (2 * dummy);
//			return (2 * side) + (2 * dummy.a);
//			return (2 * side) + (2 * dummy.a.a.a);
	}

	double area() const
	{
		return side * dummy;
//			return side * dummy.a;
//			return side * dummy.a.a.a;
	}
};



//		static void TestRectangle()
//		{
//			auto rect = new Rectangle(2.0, 4.0);
//			Console.WriteLine ("Rect size: {0}x{1}, area: {2}", rect.Side1, rect.Side2, rect.area());
//		}
//
//		static void TestSquare()
//		{
//			auto rect = new Square(4.0);
//			Console.WriteLine ("Square size: {0}, area: {1}", rect.Side1, rect.area());
//		}
//
//		static void TestSquareFer()
//		{
//			auto rect = new SquareFer(4.0);
//			Console.WriteLine ("SquareFer size: {0}, area: {1}", rect.Side, rect.area());
//		}

#include <string>
#include <iostream>
#include <chrono>
#include <vector>
#include <algorithm>

template <typename F>
void measure_and_print( std::string const& str, F f )
{
	auto start = std::chrono::steady_clock::now();

	auto res = f(); 
	// f();
 
	auto end = std::chrono::steady_clock::now();
	auto dur = std::chrono::duration_cast<std::chrono::nanoseconds>(end - start);
 
	std::cout << str << " value: " << res << " - Measured time: " << dur.count() << " ns" << std::endl;
}


// void TestTiming<T>(string text, int max, Func<int, std::vector<T>> creator, Action<std::vector<T>> processor)
// {
// 	auto list = creator(max);
// 	Console.WriteLine ("Data created, processing....");

// 	auto sw = Stopwatch.StartNew();
// 	processor (list);
// 	sw.Stop();

//     long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
//     long nanoseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L * 1000L));
//     Console.WriteLine("{0} ticks: {1}, seconds: {2}, milli: {3}, micro: {3}, nano: {3}", text, sw.ElapsedTicks, sw.Elapsed.TotalSeconds, sw.Elapsed.TotalMilliseconds, microseconds, nanoseconds);
// }



template <typename T>
std::vector<T> random_list_creator(int max)
{
	std::vector<T> list;
	list.reserve(max);

	for (int i = 0; i < max; ++i) {
		int s = std::rand() % 10000 + 0;
		//auto obj = instanceCreator(s);
		list.emplace_back(s);
	}
	return list;
}

template <typename T>
std::vector<T> random_list_creator_two_values(int max)
{
	std::vector<T> list;
	list.reserve(max);

	for (int i = 0; i < max; ++i) {
		int s1 = std::rand() % 10000 + 0;
		int s2 = std::rand() % 10000 + 0;
		list.emplace_back(s1, s2);
	}
	return list;
}

template <typename T>
std::vector<T> random_list_creator_three_values(int max)
{
	std::vector<T> list;
	list.reserve(max);

	for (int i = 0; i < max; ++i) {
		int s1 = std::rand() % 10000 + 0;
		int s2 = std::rand() % 10000 + 0;
		int s3 = std::rand() % 10000 + 0;
		list.emplace_back(s1, s2, s3);
	}
	return list;
}



template <typename T, typename F>
std::vector<T> random_list_creator_sprawled(int max, F garbageAdder)
{
	std::vector<T> list;
	list.reserve(max);

	while (list.size() < max) {
		int s = std::rand() % 10000 + 0;
		// auto obj = instanceCreator(s);

		// int x = random.Next(0, 10);
		int x = std::rand() % 10 + 0;
		if (x == 2) {
			list.emplace_back(s);
		} else {
			garbageAdder(s);
		}
	}

	return list;
}

template <typename T, typename F>
std::vector<T> random_list_creator_sprawled2(int max, F garbageAdder)
{
	std::vector<T> list;
	list.reserve(max);
    
    while (list.size() < max)
    {
        int x = std::rand() % 10 + 0;
        //if (x == 2)
        if (x % 4 == 0)
        {
            //int count = random.Next(100, 200);
            int count = std::rand() % 200 + 100;
            for (int i = 0; i < count; ++i)
            {
                int s = std::rand() % 10000 + 0;
                // auto obj = instanceCreator(s);
                garbageAdder(s);
            }
        }
        else
        {
            int s = std::rand() % 10000 + 0;
            // auto obj = instanceCreator(s);
            list.emplace_back(s);
        }
    }

    return list;
}




void Testsquare_inefficientValueTiming(int max)
{
    auto list = random_list_creator<square_inefficient>(max);

    std::cout << "Data created, processing...." << std::endl;

    measure_and_print("Testsquare_inefficientValueTiming Sort area", [&list]()  -> double {
		std::sort(begin(list), end(list), [](square_inefficient const& a, square_inefficient const& b){
			return a.area() < b.area();
		});
		return 0.0;
	});

 //    measure_and_print("Testsquare_inefficientValueTiming Sort perimeter", [&list]()  -> double {
	// 	std::sort(begin(list), end(list), [](square_inefficient const& a, square_inefficient const& b){
	// 		return a.perimeter() < b.perimeter();
	// 	});
	// 	return 0.0;
	// });

	auto m = list[max / 2];
	std::cout << "m: " << m.Side1() << ", " << m.Side2() << ", " << m.area() << std::endl;
}

void Testsquare_efficientValueTiming(int max)
{
    auto list = random_list_creator<square_efficient>(max);

    std::cout << "Data created, processing...." << std::endl;

    measure_and_print("Testsquare_efficientValueTiming Sort area", [&list]() -> double {
		std::sort(begin(list), end(list), [](square_efficient const& a, square_efficient const& b){
			return a.area() < b.area();
		});
		return 0.0;
	});

 //    measure_and_print("Testsquare_efficientValueTiming Sort perimeter", [&list]() -> double {
	// 	std::sort(begin(list), end(list), [](square_efficient const& a, square_efficient const& b){
	// 		return a.perimeter() < b.perimeter();
	// 	});
	// 	return 0.0;
	// });


	auto m = list[max / 2];
	std::cout << "m: " << m.Side() << ", " << m.area() << std::endl;
}




void Testcomposite_inefficientValueTiming(int max)
{
    auto list = random_list_creator_three_values<composite_inefficient>(max);

    std::cout << "Data created, processing...." << std::endl;

    measure_and_print("Testcomposite_inefficientValueTiming area", [&list]() -> double {
		std::sort(begin(list), end(list), [](composite_inefficient const& a, composite_inefficient const& b){
			return a.area() < b.area();
		});
		return 0;
		// auto it = std::min_element(begin(list), end(list), [](composite_inefficient const& a, composite_inefficient const& b){
		// 	return a.area() < b.area();
		// });
		// return it->area();
	});

 //    measure_and_print("Testcomposite_inefficientValueTiming perimeter", [&list]() -> double {
	// 	std::sort(begin(list), end(list), [](composite_inefficient const& a, composite_inefficient const& b){
	// 		return a.perimeter() < b.perimeter();
	// 	});
	// 	return 0;
	// 	// auto it = std::min_element(begin(list), end(list), [](composite_inefficient const& a, composite_inefficient const& b){
	// 	// 	return a.perimeter() < b.perimeter();
	// 	// });
	// 	// return it->perimeter();
	// });


	auto m = list[max / 2];
	std::cout << "m: " << m.area() << " - " << m.perimeter() << std::endl;
}

void Testcomposite_efficientValueTiming(int max)
{
    auto list = random_list_creator_three_values<composite_efficient>(max);

    std::cout << "Data created, processing...." << std::endl;

    measure_and_print("Testcomposite_efficientValueTiming area", [&list]() -> double {
		std::sort(begin(list), end(list), [](composite_efficient const& a, composite_efficient const& b){
			return a.area() < b.area();
		});
		return 0;
		// auto it = std::min_element(begin(list), end(list), [](composite_efficient const& a, composite_efficient const& b){
		// 	return a.area() < b.area();
		// });
		// return it->area();
	});

 //    measure_and_print("Testcomposite_efficientValueTiming perimeter", [&list]() -> double {
	// 	std::sort(begin(list), end(list), [](composite_efficient const& a, composite_efficient const& b){
	// 		return a.perimeter() < b.perimeter();
	// 	});
	// 	return 0;
	// 	// auto it = std::min_element(begin(list), end(list), [](composite_efficient const& a, composite_efficient const& b){
	// 	// 	return a.perimeter() < b.perimeter();
	// 	// });
	// 	// return it->perimeter();
	// });

	auto m = list[max / 2];
	std::cout << "m: " << m.area() << " - " << m.perimeter() << std::endl;
}


// void Testcomposite_efficient2ValueTiming(int max)
// {
//     auto list = random_list_creator_three_values<composite_efficient2>(max);

//     std::cout << "Data created, processing...." << std::endl;

//  //    measure_and_print("Testcomposite_efficient2ValueTiming area", [&list]() -> double  {
		
// 	// 	std::sort(begin(list), end(list), [](composite_efficient2 const& a, composite_efficient2 const& b){
// 	// 		return a.area() < b.area();
// 	// 	});
// 	// 	return 0;

// 	// 	// auto it = std::min_element(begin(list), end(list), [](composite_efficient2 const& a, composite_efficient2 const& b){
// 	// 	// 	return a.area() < b.area();
// 	// 	// });
// 	// 	// return it->area();
// 	// });

//     measure_and_print("Testcomposite_efficient2ValueTiming perimeter", [&list]() -> double  {
		
// 		std::sort(begin(list), end(list), [](composite_efficient2 const& a, composite_efficient2 const& b){
// 			return a.perimeter() < b.perimeter();
// 		});
// 		return 0;

// 		// auto it = std::min_element(begin(list), end(list), [](composite_efficient2 const& a, composite_efficient2 const& b){
// 		// 	return a.perimeter() < b.perimeter();
// 		// });
// 		// return it->perimeter();
// 	});



// 	auto m = list[max / 2];
// 	std::cout << "m: " << m.area() << " - " << m.perimeter() << std::endl;
// }


// void Testsquare_inefficientReferenceTiming_Sprawled(int max)
// {
//     auto garbage = new List<square_inefficientReference>();
//     //auto list = random_list_creator_sprawled(max, x => new square_inefficientReference(x), x => garbage.emplace_back(x));
//     auto list = random_list_creator_sprawled2(max, x => new square_inefficientReference(x), x => garbage.emplace_back(x));
    
//     //Console.WriteLine("Rectangle.finalizerExecutions: {0}", Rectangle.finalizerExecutions);
//     Console.WriteLine("list size: {0}, garbage size: {1}", list.size(), garbage.size());

//     TimedAction("Testsquare_inefficientReferenceTiming_Sprawled", () =>
//     {
//         list.Sort((x, y) => x.area().CompareTo(y.area()));
//     });

//     auto m = list[max / 2];
//     Console.WriteLine("m: {0}, {1}, {2}", m.Side1, m.Side2, m.area());
// }

// void Testsquare_efficientReferenceTiming(int max)
// {
//     auto list = random_list_creator(max, x => new square_efficientReference(x));
   
//     TimedAction("Testsquare_efficientReferenceTiming", () =>
//     {
// 		list.Sort((x, y) => x.area().CompareTo(y.area()));
// 	});

// 	auto m = list[max / 2];

// 	Console.WriteLine ("m: {0}, {1}", m.Side, m.area());
// }

// void Testsquare_efficientReferenceTiming_Sprawled(int max)
// {
//     auto garbage = new List<square_efficientReference>();
//     //auto list = random_list_creator_sprawled(max, x => new square_efficientReference(x), x => garbage.emplace_back(x));
//     auto list = random_list_creator_sprawled2(max, x => new square_efficientReference(x), x => garbage.emplace_back(x));
    
//     //Console.WriteLine("Rectangle.finalizerExecutions: {0}", Rectangle.finalizerExecutions);
//     Console.WriteLine("list size: {0}, garbage size: {1}", list.size(), garbage.size());

//     TimedAction("Testsquare_efficientReferenceTiming_Sprawled", () =>
//     {
//         list.Sort((x, y) => x.area().CompareTo(y.area()));
//     });

//     auto m = list[max / 2];
//     auto mg = garbage[max / 2];
//     Console.WriteLine("m: {0}, {1}", m.Side, m.area());
//     Console.WriteLine("mg: {0}, {1}", mg.Side, mg.area());
// }

// void Testsquare_efficientValueTiming(int max)
// {
//     auto list = random_list_creator(max, x => new square_efficientValue(x));

//     TimedAction("Testsquare_efficientValueTiming", () =>
//     {
//         list.Sort((x, y) => x.area().CompareTo(y.area()));
//     });

//     auto m = list[max / 2];
//     Console.WriteLine("m: {0}, {1}", m.Side, m.area());
// }

// void Testsquare_efficientValueTiming_Sprawled(int max)
// {
//     auto garbage = new List<square_efficientValue>();
//     //auto list = random_list_creator_sprawled(max, x => new square_efficientValue(x), x => garbage.emplace_back(x));
//     auto list = random_list_creator_sprawled2(max, x => new square_efficientValue(x), x => garbage.emplace_back(x));

//     Console.WriteLine("list size: {0}, garbage size: {1}", list.size(), garbage.size());

//     TimedAction("Testsquare_efficientValueTiming_Sprawled", () =>
//     {
//         list.Sort((x, y) => x.area().CompareTo(y.area()));
//     });

//     auto m = list[max / 2];
//     auto mg = garbage[max / 2];
//     Console.WriteLine("m: {0}, {1}", m.Side, m.area());
//     Console.WriteLine("mg: {0}, {1}", mg.Side, mg.area());

// }



// void TestSquareFer2Timing(int max)
// {
// 	auto list = random_list_creator(max, x => new SquareFer2(x));

// 	Console.WriteLine ("Data created, sorting....");

// 	auto sw = Stopwatch.StartNew();

// 	list.Sort ((x, y) => x.area().CompareTo(y.area()));
// //			auto min = list.Max(x => x.area());
// //			Console.WriteLine ("TestSquareFer2Timing Min: {0}", min);

// 	sw.Stop();

// 	Console.WriteLine ("TestSquareFer2Timing ticks: {0}, seconds: {1}", sw.ElapsedTicks, sw.Elapsed.Seconds);
// }



int main(int argc, char const *argv[])
{
	std::srand(std::time(NULL));
	int elem = std::rand() % 10 + 1;

	// std::cout << "sizeof(square_inefficient): " << sizeof(square_inefficient) << std::endl;
	// std::cout << "sizeof(square_efficient): " << sizeof(square_efficient) << std::endl;
	// std::cout << "sizeof(composite_inefficient): " << sizeof(composite_inefficient) << std::endl;
	// std::cout << "sizeof(composite_efficient): " << sizeof(composite_efficient) << std::endl;

	int max = 1000000;
	// int max = 10000000;


	// Normal tests
	Testsquare_efficientValueTiming(max);
	Testsquare_inefficientValueTiming(max);
	Testsquare_efficientValueTiming(max);



	// Composite tests
	// Testcomposite_efficient2ValueTiming(max);
	// Testcomposite_efficientValueTiming(max);
	// Testcomposite_inefficientValueTiming(max);




//			TestSquareFer2Timing(max);


    //Testsquare_inefficientReferenceTiming(max);
    //Testsquare_inefficientReferenceTiming_Sprawled(max);



    // Testsquare_efficientReferenceTiming(max);
    // Testsquare_efficientReferenceTiming(max);
    // Testsquare_efficientReferenceTiming_Sprawled(max);

    // Testsquare_efficientValueTiming(max);
    // Testsquare_efficientValueTiming_Sprawled(max);










/*
OSX
	1000000 elements
		Testsquare_inefficientValueTiming Sort area value: 0 	- Measured time: 63951157 ns
		Testsquare_efficientValueTiming Sort area value: 0 		- Measured time: 58709332 ns


OLDs

1000000 elements
	Javascript 
		Inefficient					1987038718 ns - 1.987 s                
	C++
		Testsquare_inefficientValue 	95778688 ns   - 0.095 s
		Testcomposite_efficient2ValueTiming  - Measured time: 132472007 ns 
		Testcomposite_efficientValueTiming   - Measured time: 128911279 ns 
		Testcomposite_inefficientValueTiming - Measured time: 162317639 ns 


	C#
		Testsquare_efficientReferenceTiming ticks: 2434108, seconds: 1,0409515, milli: 1040,9515, micro: 1040,9515
		Testsquare_efficientReferenceTiming_Sprawled ticks: 3358972, seconds: 1,4364716, milli: 1436,4716, micro: 1436,4716
		Testsquare_efficientValueTiming ticks: 1682267, seconds: 0,7194251, milli: 719,4251, micro: 719,4251
		Testsquare_efficientValueTiming_Sprawled ticks: 1680238, seconds: 0,7185574, milli: 718,5574, micro: 718,5574




(ns / 1000000000 = s)


Environments
	Windows
		node --version
			v0.10.28
		GCC 
		Visual Studio 2013 Release Build

Razones
	- Data cache miss
	- Store Buffer ???
	- 
*/



    return 0;
}
