// http://gescript.blogspot.com.ar/2014/06/javascript-oop-visual-method-part-ii.html
// Ecma 262 - http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-262.pdf
// Tutorial Javascript: http://www.javascriptenlightenment.com/JavaScript_Enlightenment.pdf


function rectangle(side1, side2) {
  var obj = {
    side1: function() { 
      return side1;
    },
    side2: function() { 
      return side2;
    },
    area: function() { 
      return side1 * side2;
    },
    perimeter: function() {
      return (2 * side1) + (2 * side2);
    }
  };
  return obj;
}

function square_inefficient(side) {
  var obj = {
    base: rectangle(side, side),
    side: function() {
      return this.base.side1();
    },
    area: function() {
      return this.base.area();
    },
    perimeter: function() {
      return this.base.perimeter(side, side);
    }
  };
  return obj;
}

function square_inefficient_inline(side) {
  var obj = {
    base: rectangle(side, side),
    side: function() {
      return side;
    },
    area: function() {
      return side * side;
    },
    perimeter: function() {
      return (2 * side) + (2 * side);
    }
  };
  return obj;
}


function square_efficient1(side) {
  var obj = {
    side: function() { 
      return side;
    },
    area: function() { 
      return side * side;
    },
    perimeter: function() {
      return 4 * side;
    }
  };
  return obj;
}


var SquareFer2 = function(side) {
  // this.member_variable = side;
  this.side = function() { return side; };
  this.area = function() { return side * side; };
  this.perimeter = function() { return 4 * side; };
};




//----------------------------------------------------------------------------


function measure_and_print(str, f)
{
  var time1 = process.hrtime();

  f();

  var diff = process.hrtime(time1);
  console.log(str + ' time: %d nanoseconds', diff[0] * 1e9 + diff[1]);
  // console.log('diff: sedonds: %d, %d nanoseconds', diff[0], diff[1]);
}



function get_random_side()
{
  return Math.floor((Math.random() * 100) + 1);
}

function test_rectangle() {

  var s1 = get_random_side();
  var s2 = get_random_side();

  var rect = rectangle(s1, s2);

  console.log("rectangle(side1=" + s1 + ", side2=" + s2 + ")." +
              " Area: " + rect.area() +
              " Perimeter: " + rect.perimeter());
}

function test_square_inefficient() {

  var s = get_random_side();
  var squ = square_inefficient(s);

  console.log("square_inefficient(side=" + s + ")." +
              " Area: " + squ.area() +
              " Perimeter: " + squ.perimeter());
}


function test_square_efficient1() {
  var s = get_random_side();
  var squ = square_efficient1(s);

  console.log("square_efficient1(side=" + s + ")." + " Area: " + squ.area() + " Perimeter: " + squ.perimeter());
}



//---------------------------------------------------------------------------------

function test_square_inefficient_timing(max) {

  console.log("creating data...");
  var arr = [];
  for (var i = 1; i < max; ++i) {
    var s = get_random_side();
    var squ = square_inefficient(s);
    arr.push(squ);
  }

  console.log("data created, processing...");

  // measure_and_print("test_square_inefficient_timing with area", function() {
  //   arr.sort(function(a,b) { return a.area() - b.area() } );
  // } );
  measure_and_print("test_square_inefficient_timing with perimeter", function() {
    arr.sort(function(a,b) { return a.perimeter() - b.perimeter() } );
  } );

  var m = arr[max / 2];
  console.log("m: " + m.side() + ", " + m.area());
}


function test_square_inefficient_inline_timing(max) {
  console.log("creating data...");
  var arr = [];
  for (var i = 1; i < max; ++i) {
    var s = get_random_side();
    var squ = square_inefficient_inline(s);
    arr.push(squ);
  }

  console.log("data created, processing...");

  // console.log("square_inefficient_inline array created, sorting...");
  // var time1 = process.hrtime();
  // arr.sort(function(a,b) { return a.area() - b.area() } );
  // var diff = process.hrtime(time1);
  // console.log('diff: %d nanoseconds', diff[0] * 1e9 + diff[1]);


  // measure_and_print("test_square_inefficient_inline_timing with area", function() {
  //   arr.sort(function(a,b) { return a.area() - b.area() } );
  // } );
  measure_and_print("test_square_inefficient_inline_timing with perimeter", function() {
    arr.sort(function(a,b) { return a.perimeter() - b.perimeter() } );
  } );

  var m = arr[max / 2];
  console.log("m: " + m.side() + ", " + m.area());

}



function test_square_efficient1_timing(max) {
  console.log("creating data...");
  var arr = [];
  for (var i = 1; i < max; ++i) {
    var s = get_random_side();
    var squ = square_efficient1(s);
    arr.push(squ);
  }

  console.log("data created, processing...");

  // var time1 = process.hrtime();
  // arr.sort(function(a,b) { return a.area() - b.area() } );
  // var diff = process.hrtime(time1);
  // console.log('diff: %d nanoseconds', diff[0] * 1e9 + diff[1]);


  // measure_and_print("test_square_efficient1_timing with area", function() {
  //   arr.sort(function(a,b) { return a.area() - b.area() } );
  // } );
  measure_and_print("test_square_efficient1_timing with perimeter", function() {
    arr.sort(function(a,b) { return a.perimeter() - b.perimeter() } );
  } );

  var m = arr[max / 2];
  console.log("m: " + m.side() + ", " + m.area());
}





function test_SquareFer2_timing(max) {
  console.log("creating data...");
  var arr = [];
  for (var i = 1; i < max; ++i) {
    var s = get_random_side();
    var squ = new SquareFer2(s);
    arr.push(squ);
  }

  console.log("SquareFer2 array created, sorting...");

  var time1 = process.hrtime();

  arr.sort(function(a,b) { return a.area() - b.area() } );

  var diff = process.hrtime(time1);
  console.log('diff: %d nanoseconds', diff[0] * 1e9 + diff[1]);
}






function tests() {

  // test_rectangle();
  // test_square_inefficient();
  // test_square_efficient1();

  var max = 1000000;
  // var max = 10000000;

  test_square_inefficient_timing(max);          //dummy, to "warm" the machine
  test_square_efficient1_timing(max);           //supuestamente el mas lento
  test_square_inefficient_inline_timing(max);   //supuestamente el intermedio
  test_square_inefficient_timing(max);          //supuestamente el mas rapido


  
  //test_SquareFer2_timing(max);
}


tests();


/*

OSX
  1000000 elements
    test_square_efficient1_timing with area              time: 1226736415 nanoseconds
    test_square_inefficient_inline_timing with area      time: 1609993554 nanoseconds
    test_square_inefficient_timing with area             time: 2290016742 nanoseconds


    test_square_efficient1_timing with perimeter         time: 1254184155 nanoseconds
    test_square_inefficient_inline_timing with perimeter time: 1667912724 nanoseconds
    test_square_inefficient_timing with perimeter        time: 2207813854 nanoseconds



  10000000 elements
    node js3_fer.js 
    FATAL ERROR: JS Allocation failed - process out of memory
*/


