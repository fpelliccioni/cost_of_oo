using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;


/*
Dudas:
 - Layout de los Reference Types de C#
 - Layout de los Reference Types cuando hay herencia 
 - Layout de los Value Types de C#
 - Donde son "alocados" los arrays de C#? (Aparentemente en el heap)
 - Layout de los arrays de C#
 - Layout y semantica de System.Collections.Generic.List
 - ¿Por qué la diferencia de 10x de performance en el sorting (y min) con respecto a C++?  ¿Sera por el bound-checking? Revisar!
 - Revisar la interface de List.Min() retorna un builtin type... por que??????
 - Auto properties y tipos inmutables
*/

namespace Csharp
{

    public static class MemoryAddress
    {
        public static string Get(object a)
        {
            //GCHandle handle = GCHandle.Alloc(a, GCHandleType.Pinned);
            //GCHandle handle = GCHandle.Alloc(a);
            GCHandle handle = GCHandle.Alloc(a, GCHandleType.Weak);
            IntPtr pointer = GCHandle.ToIntPtr(handle);
            handle.Free();
            return "0x" + pointer.ToString("X");
        }
    }



	//CSharp Specific: No podemos hacerlo un ValueType porque no se puede usar herencia.
	//	http://stackoverflow.com/questions/2310103/why-a-c-sharp-struct-cannot-be-inherited

	//CSharp Specific: No podemos parametrizar el tipo del Rectangulo ya que no podemos aplicar operaciones aritmeticas sobre T. Investigar.


	// Tengo que hacer dos pruebas. La primera, mostrando las desventajas de los ReferenceTypes, desparramando
	// los objetos de la lista por la memoria. Actualmente supongo que estan razonablemente contiguos, ya que uno
	// se aloca tras del otro.

	// La segunda prueba, Crear dos tipos de Square, uno eficiente y otro no.
	// Crear un contenedor, de dos Squares, y meter ese contenedor en una lista.
	// Probar esto...

    //TODO: ver .Net Object Layout in memory
        // http://geekswithblogs.net/robp/archive/2008/08/13/speedy-c-part-3-understanding-memory-references-pinned-objects-and.aspx

	class Rectangle
	{
		private double _side1;
		private double _side2;

		public double Side1 
		{
			get { return _side1; }
		}

		public double Side2
		{
			get { return _side2; }
		}

		public Rectangle(double side1, double side2)
		{
			_side1 = side1;
			_side2 = side2;
		}

		public double Perimeter()
		{
			return (2 * _side1) + (2 * _side2);
		}

		public double Area()
		{
			return _side1 * _side2;
		}


//		public static int finalizerExecutions = 0;
//		~Rectangle()
//		{
//			++finalizerExecutions;
////			Console.WriteLine("~Rectangle()");
//		}
	}

	class SquareInefficientReference : Rectangle
	{
		public SquareInefficientReference(double side) : base(side, side) {}
		//invariant (implicit): side1 == side2 
	}

    class SquareEfficientReference
    {
        private double _side;

        public double Side
        {
            get { return _side; }
        }

        public SquareEfficientReference(double side)
        {
            _side = side;
        }

        public double Perimeter()
        {
            return 4 * _side;
        }

        public double Area()
        {
            return _side * _side;
        }
    }

//	[StructLayout(LayoutKind.Sequential)]
	struct SquareEfficientValue
	{
		private double _side;

		public double Side
		{
			get { return _side; }
		}

        public SquareEfficientValue(double side)
		{
			_side = side;
		}

		public double Perimeter()
		{
			return 4 * _side;
		}

		public double Area()
		{
			return _side * _side;
		}
	}

//	[StructLayout(LayoutKind.Sequential)]
	struct BigObject1 	//4 bytes * 4 = 16 bytes
	{
		public double a;
		public double b;
		public double c;
		public double d;
	}

//	[StructLayout(LayoutKind.Sequential)]
	struct BigObject2  // 16 bytes * 4 = 64 bytes
	{
		public BigObject1 a;
		public BigObject1 b;
		public BigObject1 c;
		public BigObject1 d;
	}

//	[StructLayout(LayoutKind.Sequential)]
	struct BigObject3  // 64 bytes * 4 = 256 bytes
	{
		public BigObject2 a;
		public BigObject2 b;
		public BigObject2 c;
		public BigObject2 d;
	}

//	[StructLayout(LayoutKind.Sequential)]
	struct SquareFer2
	{
//		private BigObject3 _dummy;
//		private BigObject2 _dummy;
//		private BigObject1 _dummy;
		private double _dummy;
		private Int64 _dummy2;
		private double _side;

		public double Side
		{
			get { return _side; }
		}

		public double Dummy
		{
			get { return _dummy; }
		}
			
		public SquareFer2(double side)
		{
			_side = side;
			_dummy = side;

			_dummy2 = 0;

//			_dummy = new BigObject3 ();
//			_dummy.a.a.a = side;
//			_dummy = new BigObject1 ();
//			_dummy.a = side;
		}

		public double Perimeter()
		{
			return (2 * _side) + (2 * _dummy);
//			return (2 * _side) + (2 * _dummy.a);
//			return (2 * _side) + (2 * _dummy.a.a.a);
		}

		public double Area()
		{
			return _side * _dummy;
//			return _side * _dummy.a;
//			return _side * _dummy.a.a.a;
		}
	}


	class CompositeInefficient
	{
		private SquareInefficientReference square1;
		private SquareInefficientReference square2;
//		private SquareInefficientReference square3;

		public CompositeInefficient(double side_square1, double side_square2, double side_square3)
		{
			square1 = new SquareInefficientReference(side_square1);
			square2 = new SquareInefficientReference(side_square2);
//			square3 = new SquareInefficientReference(side_square3);
		}

		public double Area()
		{
			return square1.Area() + square2.Area(); // + square3.Area();
		}
	}

//	class CompositeEfficient
//	[StructLayout(LayoutKind.Sequential)]
	struct CompositeEfficient
	{
//		private SquareEfficientReference square1;
//		private SquareEfficientReference square2;
////		private SquareEfficientReference square3;

		private SquareEfficientValue square1;
		private SquareEfficientValue square2;
		//		private SquareEfficientValue square3;


		public CompositeEfficient(double side_square1, double side_square2, double side_square3)
		{
//			square1 = new SquareEfficientReference(side_square1);
//			square2 = new SquareEfficientReference(side_square2);
////			square3 = new SquareEfficientReference(side_square3);
			square1 = new SquareEfficientValue(side_square1);
			square2 = new SquareEfficientValue(side_square2);
//			square3 = new SquareEfficientValue(side_square3);
		}

		public double Area()
		{
			return square1.Area () + square2.Area (); // + square3.Area();
		}
	}


	class MainClass
	{
//		public static void TestRectangle()
//		{
//			var rect = new Rectangle(2.0, 4.0);
//			Console.WriteLine ("Rect size: {0}x{1}, area: {2}", rect.Side1, rect.Side2, rect.Area());
//		}
//
//		public static void TestSquare()
//		{
//			var rect = new Square(4.0);
//			Console.WriteLine ("Square size: {0}, area: {1}", rect.Side1, rect.Area());
//		}
//
//		public static void TestSquareFer()
//		{
//			var rect = new SquareFer(4.0);
//			Console.WriteLine ("SquareFer size: {0}, area: {1}", rect.Side, rect.Area());
//		}


		public static void TestTiming<T>(string text, int max, Func<int, List<T>> creator, Action<List<T>> processor)
		{
			var list = creator(max);
			Console.WriteLine ("Data created, processing....");

			var sw = Stopwatch.StartNew();
			processor (list);
			sw.Stop();

            long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
            long nanoseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L * 1000L));
            Console.WriteLine("{0} ticks: {1}, seconds: {2}, milli: {3}, micro: {3}, nano: {3}", text, sw.ElapsedTicks, sw.Elapsed.TotalSeconds, sw.Elapsed.TotalMilliseconds, microseconds, nanoseconds);
		}
        
		public static double TimedAction(string text, Func<double> processor)
		{
			Console.WriteLine ("Data created, processing....");

			var sw = Stopwatch.StartNew();
			double x = processor();
			sw.Stop();

            //Console.WriteLine ("{0} ticks: {1}, seconds: {2}", text, sw.ElapsedTicks, sw.Elapsed.Seconds);
            long microseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L));
            //long nanoseconds = sw.ElapsedTicks / (Stopwatch.Frequency / (1000L * 1000L * 1000L));
            long nanoseconds = 0;
            Console.WriteLine("{0} ticks: {1}, seconds: {2}, milli: {3}, micro: {3}", text, sw.ElapsedTicks, sw.Elapsed.TotalSeconds, sw.Elapsed.TotalMilliseconds, microseconds);

			return x;
		}
        
		public static List<T> RandomListCreator<T>(int max, Func<double, T> instanceCreator)
		{
			var list = new List<T>(max);
			Random random = new Random();

			for (int i = 0; i < max; i++) {
				int s = random.Next(0, 10000);
				var obj = instanceCreator(s);
				list.Add(obj);
			}

			return list;
		}

		public static List<T> RandomListCreatorThreeValues<T>(int max, Func<double, double, double, T> instanceCreator)
		{
			var list = new List<T>(max);
			Random random = new Random();

			for (int i = 0; i < max; i++) {
				int s1 = random.Next(0, 10000);
				int s2 = random.Next(0, 10000);
				int s3 = random.Next(0, 10000);
				var obj = instanceCreator(s1, s2, s3);
				list.Add(obj);
			}

			return list;
		}


		public static List<T> RandomListCreatorSprawled<T>(int max, Func<double, T> instanceCreator, Action<T> garbageAdder)
		{
			var list = new List<T>(max);
			Random random = new Random();

			while (list.Count < max) {
				int s = random.Next(0, 10000);
				var obj = instanceCreator(s);

				int x = random.Next(0, 10);
				if (x == 2) {
					list.Add(obj);
				} else {
					garbageAdder(obj);
				}
			}

			return list;
		}

        public static List<T> RandomListCreatorSprawled2<T>(int max, Func<double, T> instanceCreator, Action<T> garbageAdder)
        {
            var list = new List<T>(max);
            Random random = new Random();

            while (list.Count < max)
            {
                int x = random.Next(0, 10);
                //if (x == 2)
                if (x % 4 == 0)
                {
                    int count = random.Next(100, 200);
                    for (int i = 0; i < count; i++)
                    {
                        int s = random.Next(0, 10000);
                        var obj = instanceCreator(s);
                        garbageAdder(obj);
                    }
                }
                else
                {
                    int s = random.Next(0, 10000);
                    var obj = instanceCreator(s);

                    list.Add(obj);
                }
            }

            return list;
        }




		public static void TestSquareInefficientReferenceTiming(int max)
		{
			var list = RandomListCreator(max, x => new SquareInefficientReference(x));

//			List<Square> garbage = new List<Square>();
//			var list = RandomListCreatorSprawled(max, x => new Square(x), x => garbage.Add(x));
//			Console.WriteLine ("Rectangle.finalizerExecutions: {0}", Rectangle.finalizerExecutions);

            
			TimedAction("TestSquareInefficientReferenceTiming", () =>
            {
				list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
			});

			var m = list[max / 2];
			Console.WriteLine ("m: {0}, {1}, {2}", m.Side1, m.Side2, m.Area());
		}
        
		public static void TestSquareInefficientReferenceTiming_Sprawled(int max)
        {
			var garbage = new List<SquareInefficientReference>();
			//var list = RandomListCreatorSprawled(max, x => new SquareInefficientReference(x), x => garbage.Add(x));
			var list = RandomListCreatorSprawled2(max, x => new SquareInefficientReference(x), x => garbage.Add(x));
            
            //Console.WriteLine("Rectangle.finalizerExecutions: {0}", Rectangle.finalizerExecutions);
            Console.WriteLine("list size: {0}, garbage size: {1}", list.Count, garbage.Count);

			TimedAction("TestSquareInefficientReferenceTiming_Sprawled", () =>
            {
                list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
            });

            var m = list[max / 2];
            Console.WriteLine("m: {0}, {1}, {2}", m.Side1, m.Side2, m.Area());
        }

        public static void TestSquareEfficientReferenceTiming(int max)
		{
            var list = RandomListCreator(max, x => new SquareEfficientReference(x));
           
            TimedAction("TestSquareEfficientReferenceTiming", () =>
            {
				list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
			});

			var m = list[max / 2];

			Console.WriteLine ("m: {0}, {1}", m.Side, m.Area());
		}

        public static void TestSquareEfficientReferenceTiming_Sprawled(int max)
        {
            var garbage = new List<SquareEfficientReference>();
            //var list = RandomListCreatorSprawled(max, x => new SquareEfficientReference(x), x => garbage.Add(x));
            var list = RandomListCreatorSprawled2(max, x => new SquareEfficientReference(x), x => garbage.Add(x));
            
            //Console.WriteLine("Rectangle.finalizerExecutions: {0}", Rectangle.finalizerExecutions);
            Console.WriteLine("list size: {0}, garbage size: {1}", list.Count, garbage.Count);

            TimedAction("TestSquareEfficientReferenceTiming_Sprawled", () =>
            {
                list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
            });

            var m = list[max / 2];
            var mg = garbage[max / 2];
            Console.WriteLine("m: {0}, {1}", m.Side, m.Area());
            Console.WriteLine("mg: {0}, {1}", mg.Side, mg.Area());
        }

        public static void TestSquareEfficientValueTiming(int max)
		{
            var list = RandomListCreator(max, x => new SquareEfficientValue(x));

            TimedAction("TestSquareEfficientValueTiming", () =>
            {
                list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
            });

            var m = list[max / 2];
            Console.WriteLine("m: {0}, {1}", m.Side, m.Area());
		}

        public static void TestSquareEfficientValueTiming_Sprawled(int max)
        {
            var garbage = new List<SquareEfficientValue>();
            //var list = RandomListCreatorSprawled(max, x => new SquareEfficientValue(x), x => garbage.Add(x));
            var list = RandomListCreatorSprawled2(max, x => new SquareEfficientValue(x), x => garbage.Add(x));

            Console.WriteLine("list size: {0}, garbage size: {1}", list.Count, garbage.Count);

            TimedAction("TestSquareEfficientValueTiming_Sprawled", () =>
            {
                list.Sort((x, y) => x.Area().CompareTo(y.Area()));
				return 0;
            });

            var m = list[max / 2];
            var mg = garbage[max / 2];
            Console.WriteLine("m: {0}, {1}", m.Side, m.Area());
            Console.WriteLine("mg: {0}, {1}", mg.Side, mg.Area());

        }



		public static void TestSquareFer2Timing(int max)
		{
//			var list = new List<SquareFer2>(max);
//			Random random = new Random();
//
//			for (int i = 0; i < max; i++) {
//				int s = random.Next(0, 100);
//				var obj = new SquareFer2(s);
//				list.Add(obj);
//			}

			var list = RandomListCreator(max, x => new SquareFer2(x));

			Console.WriteLine ("Data created, sorting....");

			var sw = Stopwatch.StartNew();

			list.Sort ((x, y) => x.Area().CompareTo(y.Area()));
//			var min = list.Max(x => x.Area());
//			Console.WriteLine ("TestSquareFer2Timing Min: {0}", min);

			sw.Stop();

			Console.WriteLine ("TestSquareFer2Timing ticks: {0}, seconds: {1}", sw.ElapsedTicks, sw.Elapsed.Seconds);
		}





		public static void TestCompositeInefficientReferenceTiming(int max)
		{
			var list = RandomListCreatorThreeValues(max, (x, y, z) => new CompositeInefficient(x, y, z));

			var min = TimedAction("TestCompositeInefficientReferenceTiming", () =>
				{
//					list.Sort((x, y) => x.Area().CompareTo(y.Area()));
					return list.Max(x => x.Area());
				});

			Console.WriteLine ("TestCompositeInefficientReferenceTiming Min: {0}", min);

			var m = list[max / 2];
			Console.WriteLine("m: {0}", m.Area());
		}

		public static void TestCompositeEfficientReferenceTiming(int max)
		{
			var list = RandomListCreatorThreeValues(max, (x, y, z) => new CompositeEfficient(x, y, z));

			var min = TimedAction("TestCompositeEfficientReferenceTiming", () =>
				{
//					list.Sort((x, y) => x.Area().CompareTo(y.Area()));
					return list.Max(x => x.Area());
				});

			Console.WriteLine ("TestCompositeEfficientReferenceTiming Min: {0}", min);

			var m = list[max / 2];
			Console.WriteLine("m: {0}", m.Area());
		}


		public static void Main (string[] args)
		{


//			TestRectangle();
//			TestSquare ();
//			TestSquareFer ();

			int max = 10000000;

//			TestSquareFer2Timing(max);

            
			//TestSquareInefficientReferenceTiming(max);
			//TestSquareInefficientReferenceTiming(max);
			//TestSquareInefficientReferenceTiming_Sprawled(max);



//            TestSquareEfficientReferenceTiming(max);
//            TestSquareEfficientReferenceTiming(max);
//            TestSquareEfficientReferenceTiming_Sprawled(max);
//
//            TestSquareEfficientValueTiming(max);
//            TestSquareEfficientValueTiming_Sprawled(max);


			TestCompositeInefficientReferenceTiming(max);
			TestCompositeEfficientReferenceTiming(max);

            Console.ReadLine();
		}
	}
}






//            for (int i = 0; i < 10; i++) {
//                var o = list[i];


//                //GCHandle ObjGChandle = GCHandle.Alloc(o, GCHandleType.WeakTrackResurrection);
//                //int ObjAddress = GCHandle.ToIntPtr(ObjGChandle).ToInt32();
//                //System.Console.WriteLine("The address of object is : {0}", ObjAddress.ToString());
//                //ObjGChandle.Free();


//                //Console.WriteLine("i: {0}, address: 0x{1}", i, MemoryAddress.Get(o));
//                //Console.WriteLine("i: {0}, address: 0x{1}", i, MemoryAddress.Get(list[i]));

////                unsafe {
//////					TypedReference tr = __makeref(o);
//////					IntPtr ptr = **(IntPtr**)(&tr);
//////					IntPtr ptr = **(IntPtr**)(&o);
////                    IntPtr ptr = (IntPtr)(o);
////                    Console.WriteLine("i: {0}, address: 0x{1}", i, ptr.ToString("X"));
////                }
//            }