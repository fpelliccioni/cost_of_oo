# Dudas:
#   How to make an object immutable?

# Timing: http://www.daniweb.com/software-development/python/code/465991/high-resolution-time-measurement-python

# Experiencia:
#   Tuve varios errores en Runtime que en un lenguaje estatico podrian haber sido detectados prematuramente.
#   Entonces, eso de que Python es mas "productivo" que otros lenguajes estaticos, creo que es relativo.


import time
import random

# ----------------------------------------------------------------------------
# Classes
# ----------------------------------------------------------------------------

class Rectangle(object):
  # Para que sirve NEW?
  # def __new__(cls, children, *args, **kwargs):
  #   self = super(Rectangle, cls).__new__(cls)
  #   return self

  def __init__(self, s1, s2):
    super(Rectangle, self).__init__()
    self.side1 = s1
    self.side2 = s2

  def area(self):
    return self.side1 * self.side2
  def perimeter(self):
    return (2 * self.side1) + (2 * self.side2)

class SquareInefficient(Rectangle):
  def __init__(self, s):
    super(SquareInefficient, self).__init__(s, s)

class SquareEfficient(object):
  def __init__(self, s):
    super(SquareEfficient, self).__init__()
    self.side = s

  def area(self):
    return self.side * self.side
  def perimeter(self):
    return 4 * self.side

# ----------------------------------------------------------------------------
# Measuring infrastructure
# ----------------------------------------------------------------------------

def measure_and_print(str, f):

  start = time.perf_counter()

  f()

  end = time.perf_counter()
  elapsed = end - start
  print(str + " - time = {:.12f} seconds".format(elapsed))


# ----------------------------------------------------------------------------
# Helpers
# ----------------------------------------------------------------------------

def get_random_side():
  return random.randint(1, 100)


# ----------------------------------------------------------------------------
# Tests
# ----------------------------------------------------------------------------

def test_rectangle():
  s1 = get_random_side()
  s2 = get_random_side()

  rect = Rectangle(s1, s2)

  print("Rectangle(side1=" + s1 + ", side2=" + s2 + ")." +
              " Area: " + rect.area() +
              " Perimeter: " + rect.perimeter())


def test_square_inefficient():
  s = get_random_side()
  squ = SquareInefficient(s)

  print("SquareInefficient(side=" + s + ")." +
              " Area: " + squ.area() +
              " Perimeter: " + squ.perimeter())

def test_square_efficient1():
  s = get_random_side()
  squ = SquareEfficient(s)

  print("SquareEfficient(side=" + s + ")." + " Area: " + squ.area() + " Perimeter: " + squ.perimeter())

# ---------------------------------------------------------------------------------

def test_square_inefficient_timing(max):
  print("generating data...")
  arr = []
  for i in range(1, max):
    s = get_random_side()
    squ = SquareInefficient(s)
    arr.append(squ)
  
  print("data generated, processing...")

  measure_and_print("test_square_inefficient_timing with area", lambda : sorted(arr, key=lambda obj: obj.area()) )
  # measure_and_print("test_square_inefficient_timing with perimeter", lambda : sorted(arr, key=lambda obj: obj.perimeter()) )

  m = arr[max // 2]
  # print("m: %d, %d" % (m.side1, m.area())
  print("(m: {}, ${})".format(m.side1, m.area()))

def test_square_efficient_timing(max):
  print("generating data...")

  arr = []
  for i in range(1, max):
    s = get_random_side()
    squ = SquareEfficient(s)
    arr.append(squ)
  
  print("data generated, processing...")

  measure_and_print("test_square_efficient_timing with area", lambda : sorted(arr, key=lambda obj: obj.area()) )
  # measure_and_print("test_square_efficient_timing with perimeter", lambda : sorted(arr, key=lambda obj: obj.perimeter()) )

  m = arr[max // 2]
  # print("m: %d, %d" % (m.side, m.area())
  print("(m: {}, ${})".format(m.side, m.area()))



def tests():
  # test_rectangle()
  # test_square_inefficient()
  # test_square_efficient1()

  max = 1000000
  # max = 10000000

  # test_square_inefficient_timing(max)
  test_square_inefficient_timing(max)
  test_square_efficient_timing(max)



def main():
  tests()

if __name__ == "__main__":
  main()



# OSX
#   1000000 elements
#     test_square_inefficient_timing with perimeter - time = 0.680876275001 seconds
#     test_square_efficient_timing with perimeter   - time = 0.601360634988 seconds
#     test_square_inefficient_timing with area      - time = 0.789165196009 seconds
#     test_square_efficient_timing with area        - time = 0.722813372995 seconds



#   10000000 elements
# 
#     test_square_inefficient_timing with area - time = 7.281202705024 seconds
#     test_square_efficient_timing with area -   time = 7.262475820986 seconds


