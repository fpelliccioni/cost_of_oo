*************************************
 Cost of Object Oriented Programming
*************************************

Motivation

*********
The tests
*********

1) complete
2) complete


*********
Platforms
*********

1) Processor: Intel(R) Core(TM) i7-3720QM CPU 2.6 GHz - Ivy Bridge microarchitecture.
   
   Memory: 8GB 1600 MHz DDR3
   
   Operating system: Mac OS X 10.9.2

   C++ compiler: clang version 3.5 (trunk 200620)
      clang++ -O3 -std=c++11 file_name.cpp

   C# compiler: Xamarin Version? MonoVersion?
      (Compilation mode?)

   Javascript interpreter: nodeJs V8 version?
      (Compilation mode?)

   Python interpreter: python 2.7.5
      python file_name.py

   Eiffel compiler: (No used on OSX)

2) Processor: Intel(R) Core(TM) i7-4700MQ CPU 2.4 GHz - Haswell microarchitecture.
   
   Memory: 8GB 2400 MHz DDR3
   
   Operating system: Windows 8.1

   C++ compiler: GCC (MinGW)  version 4.8.0
      g++ -O3 -std=c++11 file_name.cpp

   C# compiler: Visual Studio 2013 (specific version?)
      (Compilation mode?)

   Javascript interpreter: nodeJs V8 version?
      (Compilation mode?)

   Python interpreter: python 2.7.5
      python file_name.py

   Eiffel compiler: Eiffel Studio 14.05 ( specific version? )
      (Compilation mode?)



Other languages to test:
   D
   Rust
   Go
   Haskell
   Smalltalk
   Ruby